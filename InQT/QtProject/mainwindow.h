#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "admin.h"
#include <QMainWindow>
#include <QWebFrame>
#include <QWebElement>
#include <iostream>

using namespace  std;
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    QString setToPointsPolyToDB(QString);
    QString getPointsRightToDbs();

    explicit MainWindow(QWidget *parent = 0);

    ~MainWindow();

private slots:
    void on_pushButton_clicked(); //this is a function with no specific function in program yet.

    void on_refreshButton_clicked(); //this is SLOT just to refresh the map (like f5 in browser) is a button REFRESH

    void on_CreatePolygonPoints_clicked();

public slots:
    void getPointsOfPolygonHtml(); //this a SLOT  that would be exist like function in one Object in javascript(js)
    void sendPointFigToJs();

private:
    Ui::MainWindow *ui;
    Admin* main;   //this object is a call to admin .. because in this object "MainWindow" is where everithing finish, riht to show
    QString PointsRightToDbs;
    Polygon *OnePolygon; //Polygon to Work in mainwindow
    QString latitudePointFig;
    QString longitudePointFig;
};

#endif // MAINWINDOW_H
