#ifndef POLYGON_H
#define POLYGON_H
#include <QString>

class Polygon
{
    public:
        Polygon();
        void setPoints(QString);
        QString getPoints();

    private:
        QString PointsPolygon;
};

#endif // POLYGON_H
