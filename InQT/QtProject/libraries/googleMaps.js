/********(S)->means START Code*********/
/********(E)->means END Code***********/

/***IMPORTANT!!!(S)here we use the object passed in constructor of MainWindow(c++) with function
"addToJavaScriptWindowObject"****/
function runFunctionOfCplusplus()
{
    myObject.getPointsOfPolygonHtml(); //and here we use the function implemented in c++
}
/***IMPORTANT!!!(E)here we use the object passed in constructor of MainWindow(c++) with function
"addToJavaScriptWindowObject"****/

/******(S)here we handle the html to put points got it by event 'dragend'******/
function putDataInHtml(event) {

    // Since this polygon has only one path, we can call getPath()
    // to return the MVCArray of LatLngs.
    var vertices = this.getPath();

    var contentString = '';

    var xml = "<rss version='2.0'><coordinates></coordinates></rss>",
    xmlDoc = $.parseXML( xml ),
    $xml = $( xmlDoc ),
    $title = $xml.find( "coordinates" );

    // Iterate over the vertices.
    for (var i =0; i < vertices.getLength(); i++)
    {
        var xy = vertices.getAt(i);
        contentString +=xy.lat()+" "+xy.lng()+"\n";
    }
    console.log(contentString);
    $title.text( contentString);
    $( "#anotherElement" ).empty();
    $( "#anotherElement" ).append( $title.text() );

    runFunctionOfCplusplus();
}
/******(S)here we handle the html to put points got it by event 'dragend'******/

var rendererOptions = {
  draggable: true,
  suppressMarkers: true
};
var directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);;
var directionsService = new google.maps.DirectionsService();
var arequipa = new google.maps.LatLng(-16.39003,-71.535829);
var map;
var markerClusterer = null; //for clustere the points within circle
var persons = []; //for persons points
var destinations = []; //for persons points
var MarkerPersons = []; //for persons points
var MarkerDestinations = []; //for persons points
var paraderoIcon = new google.maps.MarkerImage(
        "./images/parking.png",
        null, null, null, new google.maps.Size(25, 25));
var personIcon = new google.maps.MarkerImage(
        "./images/person.png",
        null, null, null, new google.maps.Size(25, 25));
var DestinationIcon = new google.maps.MarkerImage(
        "./images/destination.png",
        null, null, null, new google.maps.Size(25, 25));



var polylineOptionsRoutes = {
strokeColor: '#FF0000',
strokeOpacity: 1.0,
strokeWeight: 10
};

var polylineOptionsTest = {
strokeColor: '#00FF00',
strokeOpacity: 1.0,
strokeWeight: 5
};


/*(S)*************INITIALIZE**************************/
function initialize() //  this is most important funcion that is colled in the end
{

  var mapOptions = {
    zoom:16,
    center: arequipa
    }
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);


    /*****(S)here we initialize a  polygon by defect in google maps********/
     var triangleCoords = [
        new google.maps.LatLng(-16.390416071738024, -71.53626892394254 ),
        new google.maps.LatLng(-16.390526492315352, -71.53544946560669),
        new google.maps.LatLng(-16.38981348571007, -71.53532738161476),
         new google.maps.LatLng(-16.389718485649876, -71.53613238161478),
      ];
    // Construct the polygon.
      bermudaTriangle = new google.maps.Polygon({
        paths: triangleCoords,
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35,
        draggable:true,
        editable: true,
        clickable: true,
        zIndex:0,
        //map:map, // same (1)
      });
    //  bermudaTriangle.setMap(map); // same (1)
    /*****(E)here we initialize a  polygon by defect in google maps********/
    google.maps.event.addListener(bermudaTriangle, 'dragend', putDataInHtml); // here use the EVENT "dragend" to call "putDataInHtml"

    /***(S)   -> Creating a Circle with buton to create a zone with people ****/
       var centerCoords= new google.maps.LatLng(-16.390413,-71.533163);

       cityCircle = new google.maps.Circle({
           center: centerCoords,
           strokeColor: '#FF0000',
           strokeOpacity: 0.8,
           strokeWeight: 2,
           fillColor: '#FF00001',
           fillOpacity: 0.35,
           draggable:true,
           editable: true,
           clickable: true,
           zIndex:1,
           map:map, // same(2)
            radius:90,
     });
   //    circleTest.setMap(map); //same(2)

/***(S)   -> Creating a Circle with buton to create a zone with Destinations ****/


       DestinationsCircle = new google.maps.Circle({
           center: new google.maps.LatLng(-16.390413,-71.533163),
           strokeColor: '#FFF000',
           strokeOpacity: 0.8,
           strokeWeight: 2,
           fillColor: '#FF00001',
           fillOpacity: 0.35,
           draggable:true,
           editable: true,
           clickable: true,
           zIndex:1,
           map:map, // same(2)
            radius:150,
     });
   //    circleTest.setMap(map); //same(2)

    function PersonCircleFunToPass(event)
    {
        var latOne = cityCircle.getBounds().getNorthEast().lat();
        latOne=Math.abs(latOne);
        var lngOne = cityCircle.getBounds().getNorthEast().lng();
        lngOne =Math.abs(lngOne );
        var latTwo = cityCircle.getBounds().getSouthWest().lat();
        latTwo =Math.abs(latTwo );
        var lngTwo = cityCircle.getBounds().getSouthWest().lng();
        lngTwo =Math.abs(lngTwo );
        persons=makingRandomPoints(latOne,latTwo,lngOne,lngTwo,30);
        for(var u=0; u < persons.length ; ++u){
            var marker = new google.maps.Marker({
                 position: persons[u] ,
                 map: map,
                 title: 'Points.',
                 icon: personIcon

             });
            MarkerPersons.push(marker);
        }
    }

    function DestinationCircleFunToPass(event)
    {
        var latOne = DestinationsCircle.getBounds().getNorthEast().lat();
        latOne=Math.abs(latOne);
        var lngOne = DestinationsCircle.getBounds().getNorthEast().lng();
        lngOne =Math.abs(lngOne );
        var latTwo = DestinationsCircle.getBounds().getSouthWest().lat();
        latTwo =Math.abs(latTwo );
        var lngTwo = DestinationsCircle.getBounds().getSouthWest().lng();
        lngTwo =Math.abs(lngTwo );
        destinations=makingRandomPoints(latOne,latTwo,lngOne,lngTwo,5);
        for(var u=0; u < destinations.length ; ++u){
            var marker = new google.maps.Marker({
                 position: destinations[u] ,
                 map: map,
                 title: 'Points.',
                 icon: DestinationIcon

             });
            MarkerDestinations.push(marker);
        }
    }

    google.maps.event.addListener(cityCircle, 'dragend', PersonCircleFunToPass);
    google.maps.event.addListener(DestinationsCircle, 'dragend', DestinationCircleFunToPass);
    /***(E)   -> Creating a Circle with buton to create a zone with people or places ****/

    /***(S) -> InfoWindow of point, with double click *****/
    function toShowInfoOfPoint(latLng)
    {
        console.log(String( latLng));
        var coordInfoWindow = new google.maps.InfoWindow(
         {
            position:latLng,
            content: String( latLng),
          });

        coordInfoWindow.open(map);
    }

    google.maps.event.addListener(map, 'dblclick', function(event) {
        console.log("in dbclick");
        toShowInfoOfPoint(event.latLng);
    });
    /***(S) -> InfoWindow of point, with double click *****/
//aqui eventos
    var gParadero = document.getElementById('gParadero');
    google.maps.event.addDomListener(gParadero, 'click', get_Paradero);

    var gRoutes = document.getElementById('gRoutes');
    google.maps.event.addDomListener(gRoutes, 'click', get_routes);

    var clear = document.getElementById('clear');
    google.maps.event.addDomListener(clear, 'click', setDestination);//clearClusters);
/***(S) here we use the part to call file .kml and get points since DB, but now is
 not working by moment, now need get Polygons**/
  /*var kmlUrl = 'https://dl.dropboxusercontent.com/u/21354927/points.kml';

    var kmlOptions = {
     map: map
    };

  var ctaLayer = new google.maps.KmlLayer(kmlUrl,kmlOptions);*/
/***(E) here we use the part to call file .kml and get points since DB, but now is
 not working by moment, now need get Polygons**/
}
/*(E)*************INITIALIZE****************************************/
/*(S)*************SOME FUNCTIONS TO CALCULATES ROUTES***************/

function get_routes(){
    directionsDisplay.setMap(map);
    var start = destinations[0];
    var end = destinations[destinations.length-1];
    var waypts = [];

    for(var u=0;u<destinations.length-1;++u)    {
        var latLng = new google.maps.LatLng(destinations[u]);
        waypts.push({
                    location: latLng,
                    stopover: true});

            calcRoute(destinations[u],destinations[u+1]);
    }

}


function renderDirections(result) {
  var directionsRenderer = new google.maps.DirectionsRenderer({suppressMarkers: true, polylineOptions: polylineOptionsRoutes});
  directionsRenderer.setMap(map);
  directionsRenderer.setPanel(document.getElementById('directionsPanel'));
  directionsRenderer.setDirections(result);
}

function calcRoute(start,end) { // THIS FUNCTION COMPUTE THE ROUTES

    var request = {
        origin: start,
        destination: end,
        //waypoints: waypts,
        //optimizeWaypoints: false,
        travelMode: google.maps.DirectionsTravelMode.DRIVING
      };
    directionsService.route(request, function(result, status) {
        if (status == google.maps.DirectionsStatus.OK) {
          renderDirections(result);
        }
      });
}

/***(S)  -> Making points , randomly ****/
function makingRandomPoints(latOne, latTwo, lngOne, lngTwo, tam)
{
    console.log("here we inside random");
    var i;
    var array=[];
    for(i=0;i<tam;i++)
    {
        var lat = (Math.random() * (latOne - latTwo) + latTwo)
        lat = lat * -1;

        var long = (Math.random() * (lngOne - lngTwo) + lngTwo)
        long= long* -1;
        console.log(lat,long);
        var myLatlng = new google.maps.LatLng(lat, long);
        array.push(myLatlng);
    }
    return array;
}
/***(E)  -> Making points of people, randomly ****/

// create destination points
function setDestination()   {

    var bounds = map.getBounds();
    var southWest = bounds.getSouthWest();
    var northEast = bounds.getNorthEast();
    var lngSpan = northEast.lng() - southWest.lng();
    var latSpan = northEast.lat() - southWest.lat();
    for (var i = 0; i < 200; i++) {
      var point = new google.maps.LatLng(southWest.lat() + latSpan * Math.random(),
          southWest.lng() + lngSpan * Math.random());
      var marker = new google.maps.Marker({
             position: point ,
             map: map,
             title: 'Points.',
             icon: {
                      path: google.maps.SymbolPath.CIRCLE,
                      scale: 4
                    }
         });

    }
}


function computeTotalDistance(result) { // THIS FUNCTION COMPUTE THE DISTANCE
  var total = 0;
  var myroute = result.routes[0];
  for (var i = 0; i < myroute.legs.length; i++) {
    total += myroute.legs[i].distance.value;
  }
  total = total / 1000.0;
  document.getElementById('total').innerHTML = total + ' km';
}

//borrar marcas
function clearClusters(e) {
        e.preventDefault();
        e.stopPropagation();
        if (markerClusterer) {
          markerClusterer.clearMarkers();
        }
}
//clustered persons points
function get_Paradero() { // THIS FUNCTION COMPUTE THE ROUTES

  markerClusterer = new MarkerClusterer(map, MarkerPersons, {
          gridSize: 150
        });
}



//compute distances between two points
function distanceBetweenPoints(p1, p2) {
  if (!p1 || !p2) {
    return 0;
  }

  var R = 6371; // Radius of the Earth in km
  var dLat = (p2.lat() - p1.lat()) * Math.PI / 180;
  var dLon = (p2.lng() - p1.lng()) * Math.PI / 180;
  var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(p1.lat() * Math.PI / 180) * Math.cos(p2.lat() * Math.PI / 180) *
    Math.sin(dLon / 2) * Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c;
  return d;
};

/*(E)*************SOME FUNCTIONS**************************/
google.maps.event.addDomListener(window, 'load', initialize); // here we call the main function to initialized

