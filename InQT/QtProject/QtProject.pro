#-------------------------------------------------
#
# Project created by QtCreator 2014-04-16T10:59:59
#
#-------------------------------------------------

QT += core gui webkitwidgets
QT += core gui
QT +=sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtProject
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    query.cpp \
    admin.cpp \
    conexion.cpp \
    polygon.cpp

HEADERS  += mainwindow.h \
    query.h \
    admin.h \
    conexion.h \
    polygon.h

FORMS    += mainwindow.ui

OTHER_FILES += \
    map.html \
    templatePoints.kml \
    googleMaps.js \
    jquery.js \
    libraries/googleMaps.js \
    libraries/markerclusterer.js
